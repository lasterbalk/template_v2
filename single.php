
<?php get_header(); ?>
<?php echo get_the_post_thumbnail();  ?>
<div class="wrapper">
    <div class="breadcrumb">
        <a  href="http://localhost:8888/theme_tutorial/beispiel-seite/"><- zurück zur Übersicht</a> 
    </div>
<?php

if ( have_posts() ) :
	while ( have_posts() ) : the_post(); ?>
	
        <h2><?php the_title() ?></h2>
        <p>Autor: <?php the_author(); ?> </p>
		<?php the_content() ?>

	<?php endwhile;
else :
	echo '<p>There are no posts!</p>';
 
endif;
 
?>
</div>