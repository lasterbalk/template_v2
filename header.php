<!DOCTYPE html>
<html <?php language_attributes(); ?>>
 
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <title><?php bloginfo( 'name' ); ?></title>
    <?php wp_head() ?>
</head>
 
<body <?php body_class(); ?>>
 
<header class="site-header">
    <div class="header-left">
        <h1><a href="<?php echo home_url(); ?>"><?php bloginfo( 'name' ); ?></a></h1>
        <h4><?php bloginfo( 'description' ); ?></h4>
    </div>
    <div class="header-menu">
        <?php
        wp_nav_menu( array( 
            'theme_location' => 'my-custom-menu', 
            'container_class' => 'custom-menu-class' ) ); 
        ?>
    </div>
</header>