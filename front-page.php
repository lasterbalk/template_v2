<html>
<head>
    <title>League Template</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE-edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Blog Site Template">
    <meta name="author" content="https://leagueoflegends.com">
    <link rel="shortcut icon" href="images/logo.png">
    <!--<link rel="stylesheet" href="wp-content/themes/league/style.css">-->
    <?php wp_head(); ?>
     

</head>
<body>
    <?php get_header(); ?>
    <?php echo get_the_post_thumbnail();  ?>

     <?php get_template_part('inc/section','content'); ?>
     

    <?php get_footer(); ?>
</body>


</html>