<?php /* Template Name: API Page */

get_header();

?>

<div class="post-container" style="width:80%; margin:0 auto;">


<script>
    const url = 'http://localhost/wordpress/wp-json/wp/v2/posts';
    const postsContainer = document.querySelector('.post-container');

    fetch(url)
    .then(response => response.json())
    .then(data => {
        data.map( post => {
            const innerContent = 
            `
            <div class="post-box" style="background:orange; margin:24px 0px; padding:32px;">
            <li style="list-style:none;">
             <h2>${post.title.rendered}</h2>
             ${post.excerpt.rendered}
             <a href="${post.link}">Read More</a>
            </li>
            `
            postsContainer.innerHTML += innerContent;
        })
    });
</script>

